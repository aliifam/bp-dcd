const lovar = require('lodash');

const myOddEventArray = lovar.partition([1,2,3,4,5,6,7,8], n => n % 2 == 0);

console.log(myOddEventArray);