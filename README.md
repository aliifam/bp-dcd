Pada akhir modul ini, diharapkan Anda dapat:

- Mengetahui apa itu Node.js
- Mengeksekusi JavaScript dengan Node.js
- Mengetahui Node.js Global Object
- Memahami Modularization
- Menggunakan Node Package Manager
- Memahami Eventing
- Memahami Filesystem
- Memahami teknik Readable Stream dan Writable Stream

# NodeJS global object

- module : digunakan untuk sistem modularisasi pada Node.js.
- __filename : keyword untuk mendapatkan lokasi berkas JavaScript yang dieksekusi. Keyword ini tidak tersedia pada Node.js REPL.
- __dirname : keyword untuk mendapatkan root directory dari berkas JavaScript yang dieksekusi.
require : digunakan untuk mengimpor module JavaScript.

> Node.js menyediakan core modules fs yang dapat mempermudah kita dalam mengakses filesystem. Setiap method yang ada di module fs tersedia dalam dua versi, yakni versi asynchronous (default) dan versi synchronous.