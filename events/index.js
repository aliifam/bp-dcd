const {EventEmitter} = require('events');

const myevent = new EventEmitter();

const ultah = (person) => {
    console.log(`angka pada umur anda bertambah ${person}.`);
}

myevent.on('the day', ultah);

myevent.emit('the day', 'aliif');