const { EventEmitter } = require('events');

const myevent = new EventEmitter();

const makeorder = ({nama}) => {
    console.log(`orderan ${nama} berhasil dibuat!`);
}

const makebill = ({harga}) => {
    console.log(`bill sebesar Rp.${harga} telah dibuat!`);
}

myevent.on('buat-pesanan', makeorder);
myevent.on('buat-pesanan', makebill);

myevent.emit('buat-pesanan', {nama: 'Meja', harga: 150000});