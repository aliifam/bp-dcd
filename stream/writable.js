const fs = require('fs');
const {resolve} = require('path');

const writableStream = fs.createWriteStream('./stream/keluaran.txt');

writableStream.write('ini baris satu \n');
writableStream.write('ini baris dua \n');
writableStream.write('ini baris tiga \n');

writableStream.write('ini baris empat \n');

writableStream.end("akhir writable stream!");